CREATE TABLE emp (
    empid Integer primary key auto_increment, 
    name varchar(50),
    salary Float,
    age Integer
);


INSERT INTO emp (name, salary, age) VALUES ('a', 2222, 22);
INSERT INTO emp (name, salary, age) VALUES ('b', 2222, 23);
INSERT INTO emp (name, salary, age) VALUES ('c', 2222, 24);
INSERT INTO emp (name, salary, age) VALUES ('d', 2222, 25);
INSERT INTO emp (name, salary, age) VALUES ('e', 2222, 26);