const express= require('express')
const db= require('../db')
const utils= require('../utils')

const router= express.Router()


router.get('/',(request,response)=>{

const query= `
select * from emp
`
db.execute(query, (error,result)=>{

response.send(utils.createResult(error,result))
})

})


router.post('/',(request,response)=>{

    const {name, salary, age}= request.body
    
    const query= `
    INSERT INTO emp (name, salary, age)
    VALUES ('${name}','${salary}','${age}') 
    `
    db.execute(query, (error,result)=>{
    
    response.send(utils.createResult(error,result))
    })
    
    })


    router.put('/:id',(request,response)=>{

        const{ id }= request.params
        const {name, salary, age}= request.body
        
        const query= `
        UPDATE emp SET
        name= '${name}', 
        salary= '${salary}',
        age= '${age}'
        WHERE
        empid= '${id}'
        `
        db.execute(query, (error,result)=>{
        
        response.send(utils.createResult(error,result))
        })
        
        })


        router.delete('/:id',(request,response)=>{

            const{ id }= request.params
            
            const query= `
            DELETE FROM emp 
            WHERE
            empid= '${id}'
            `
            db.execute(query, (error,result)=>{
            
            response.send(utils.createResult(error,result))
            })
            
            })






module.exports= router